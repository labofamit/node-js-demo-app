// const http = require('http');
// const fs= require('fs');
// const path=require('path');
// const url=path.join(__dirname);
// fs.writeFileSync(`${url}demo.txt`,'fdswdswdfjdssdfjizxkjcasdhjacasd')
// //console.log(url);
// http.createServer((req,resp)=>{
//     resp.writeHead(200,{'Content_Type':'application\json'})
//     resp.write(JSON.stringify({'name':'Amit','email':'amit@gmail.com'}))
//     resp.end();
// }).listen(5000);


// page url with exoress js
const express=require('express');
const app=express();

// app.get('',(req,resp)=>{
//     resp.send('This is the home page using node js and express ja');
// })

// app.get('/about',(req,resp)=>{
//     resp.send('About This is the About Us page using node js and express ja')
// })

// app.get('/contact',(req,resp)=>{
//     resp.send('Contact This is the contact page using node js and express ja')
// })

// app.get('/demo',(req,resp)=>{
//     resp.send(`this demo page for query string ${req.query.name}`)
//     console.log(req.query); 
// })

app.listen(5000);

const { MongoClient } = require('mongodb');
// or as an es module:
// import { MongoClient } from 'mongodb'

// Connection URL
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);

// Database Name
const dbName = 'NodeDemo';

async function getData() {
  // Use connect method to connect to the server
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);
  const collection = db.collection('FreindsList');
  const  responsdata=await collection.find({}).toArray();
  console.log(responsdata);
  // the following code examples can be pasted here...

  return 'done.';
}
getData();